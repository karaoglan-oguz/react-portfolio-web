import React, { Component } from 'react';


class Section4 extends Component{

    render() {
    return (


        <section className="probootstrap-section probootstrap-bg-light" data-section="reviews">
        <div className="container">
          <div className="row text-center mb100">
            <div className="col-md-8 col-md-offset-2 probootstrap-section-heading">
              <h2 className="mb30 text-black probootstrap-heading">That’s why 100,000+ Love Frame</h2>
              <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
            </div>
          </div>
          <div className="row">
            <div className="col-md-4 col-sm-6 col-xs-12">
              <div className="probootstrap-testimonial">
                <p><img src="img/person_1.jpg" className="img-responsive img-circle probootstrap-author-photo" alt="Free Template by uicookies.com" /></p>
                <p className="mb10 probootstrap-rate">
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                </p>
                <blockquote>
                  <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
                </blockquote>
                <p className="mb0">&mdash; Garry Alexander</p>
              </div>
            </div>
            <div className="col-md-4 col-sm-6 col-xs-12">
              <div className="probootstrap-testimonial">
                <p><img src="img/person_2.jpg" className="img-responsive img-circle probootstrap-author-photo" alt="Free Template by uicookies.com" /></p>
                <p className="mb10 probootstrap-rate">
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                </p>
                <blockquote>
                  <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                </blockquote>
                <p className="mb0">&mdash; James Robertson</p>
              </div>
            </div>
            <div className="clearfix visible-sm-block"></div>
            <div className="col-md-4 col-sm-6 col-xs-12">
              <div className="probootstrap-testimonial">
                <p><img src="img/person_3.jpg" className="img-responsive img-circle probootstrap-author-photo" alt="Free Template by uicookies.com" /></p>
                <p className="mb10 probootstrap-rate">
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                </p>
                <blockquote>
                  <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
                </blockquote>
                <p className="mb0">&mdash; Ben Goodrich</p>
              </div>
            </div>
            <div className="col-md-4 col-sm-6 col-xs-12">
              <div className="probootstrap-testimonial">
                <p><img src="img/person_4.jpg" className="img-responsive img-circle probootstrap-author-photo" alt="Free Template by uicookies.com" /></p>
                <p className="mb10 probootstrap-rate">
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                  <i className="icon-star-outlined"></i>
                </p>
                <blockquote>
                  <p>And if she hasn’t been rewritten, then they are still using her.</p>
                </blockquote>
                <p className="mb0">&mdash; Kip Hugh</p>
              </div>
            </div>
            <div className="clearfix visible-sm-block"></div>
            <div className="col-md-4 col-sm-6 col-xs-12">
              <div className="probootstrap-testimonial">
                <p><img src="img/person_2.jpg" className="img-responsive img-circle probootstrap-author-photo" alt="Free Template by uicookies.com" /></p>
                <p className="mb10 probootstrap-rate">
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                </p>
                <blockquote>
                  <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                </blockquote>
                <p className="mb0">&mdash; James Robertson</p>
              </div>
            </div>
            <div className="col-md-4 col-sm-6 col-xs-12">
              <div className="probootstrap-testimonial">
                <p><img src="img/person_3.jpg" className="img-responsive img-circle probootstrap-author-photo" alt="Free Template by uicookies.com" /></p>
                <p className="mb10 probootstrap-rate">
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                  <i className="icon-star"></i>
                </p>
                <blockquote>
                  <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
                </blockquote>
                <p className="mb0">&mdash; Ben Goodrich</p>
              </div>
            </div>
  
          </div>
        </div>
      </section>
    );
}
}
export default Section4;