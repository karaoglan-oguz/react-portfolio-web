import React, { Component } from 'react';

class Section1 extends Component {

  render() {
    return (


<section className="probootstrap-section">
<div className="container">
  <div className="row">
    <div className="col-md-4">
      <div className="probootstrap-service-item" >
        <span className="icon icon-phone3"></span>
        <h2>Mobile Optimize</h2>
        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
        <p><a href="#" className="probootstrap-link">Learn More <i className="icon-chevron-right"></i></a></p>
      </div>
    </div>
    <div className="col-md-4">
      <div className="probootstrap-service-item">
        <span className="icon icon-wallet2"></span>
        <h2>Increase Revenue</h2>
        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
        <p><a href="#" className="probootstrap-link">Learn More <i className="icon-chevron-right"></i></a></p>
      </div>
    </div>
    <div className="col-md-4">
      <div className="probootstrap-service-item">
        <span className="icon icon-lightbulb"></span>
        <h2>Smart Idea</h2>
        <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
        <p><a href="#" className="probootstrap-link">Learn More <i className="icon-chevron-right"></i></a></p>
      </div>
    </div>
  </div>
</div>
</section>

    );
}
}

export default Section1;