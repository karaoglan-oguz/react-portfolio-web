import React, { Component } from 'react';


class Contact extends Component{

    render()  {
    return (

        <section className="probootstrap-section probootstrap-bg-light" data-section="contact">
        <div className="container">
          <div className="row">
            <div className="col-md-6">
              <form action="" className="probootstrap-form">
                <h2 className="text-black mt0">Get In Touch</h2>
                <div className="form-group">
                  <input type="text" className="form-control" placeholder="Your Name" />
                </div>
                <div className="form-group">
                  <input type="email" className="form-control" placeholder="Your Email" />
                </div>
                <div className="form-group">
                  <input type="email" className="form-control" placeholder="Your Phone" />
                </div>
                <div className="form-group">
                  <textarea className="form-control"cols="30" rows="10" placeholder="Write a Message"></textarea>
                </div>
                <div className="form-group">
                  <input type="submit" className="btn btn-primary" value="Sebd Message" />
                </div>
              </form>
            </div>
            <div className="col-md-3 col-md-push-1">
              <ul className="probootstrap-contact-details">
                <li>
                  <span className="text-uppercase">Email</span>
                  <p>probootstrap@gmail.com</p>
                </li>
                <li>
                  <span className="text-uppercase">Phone</span>
                  <p>+30 976 1382 9921</p>
                </li>
                <li>
                  <span className="text-uppercase">Fax</span>
                  <p>+30 976 1382 9922</p>
                </li>
                <li>
                  <span className="text-uppercase">Address</span>
                  San Francisco, CA <br/>
                  4th Floor8 Lower  <br/>
                  San Francisco street, M1 50F
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
    );
}
}
export default Contact;