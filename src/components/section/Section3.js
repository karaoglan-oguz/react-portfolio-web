import React, { Component } from 'react';


export default class Section3 extends Component{

    render() {
    return (

 <section className="probootstrap-section" data-section="pricing">
 <div className="container">
   <div className="row text-center mb100">
     <div className="col-md-8 col-md-offset-2 probootstrap-section-heading">
       <h2 className="mb30 text-black probootstrap-heading">Choose the plan that’s right for your business </h2>
       <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
     </div>
   </div>

   <div className="row">
     <div className="col-md-4">
       <div className="probootstrap-pricing">
         <h2>Starter</h2>
         <p className="probootstrap-price"><strong>$22.99</strong></p>
         <p className="probootstrap-note">This is a monthly recurring payment.</p>
         <ul className="probootstrap-list text-left mb50">
           <li className="probootstrap-check">Pointing has no control</li>
           <li className="probootstrap-check">A small river named Duden flows</li>
           <li className="probootstrap-check">Roasted parts of sentences fly into your mouth</li>
         </ul>
         <p><a href="#" className="btn btn-black">Get Started</a></p>
       </div>
     </div>
     <div className="col-md-4">
       <div className="probootstrap-pricing probootstrap-popular probootstrap-shadow">
         <h2>Business</h2>
         <p className="probootstrap-price"><strong>$69.99</strong></p>
         <p className="probootstrap-note">This is a monthly recurring payment.</p>
         <ul className="probootstrap-list text-left mb50">
           <li className="probootstrap-check">Pointing has no control</li>
           <li className="probootstrap-check">A small river named Duden flows</li>
           <li className="probootstrap-check">Roasted parts of sentences fly into your mouth</li>
         </ul>
         <p><a href="#" className="btn btn-primary">Get Started</a></p>
       </div>
     </div>
     <div className="col-md-4">
       <div className="probootstrap-pricing">
         <h2>Premium</h2>
         <p className="probootstrap-price"><strong>$224.99</strong></p>
         <p className="probootstrap-note">This is a monthly recurring payment.</p>
         <ul className="probootstrap-list text-left mb50">
           <li className="probootstrap-check">Pointing has no control</li>
           <li className="probootstrap-check">A small river named Duden flows</li>
           <li className="probootstrap-check">Roasted parts of sentences fly into your mouth</li>
         </ul>
         <p><a href="#" className="btn btn-black">Get Started</a></p>
       </div>
     </div>
   </div>
 </div>
</section>

    );
}
}