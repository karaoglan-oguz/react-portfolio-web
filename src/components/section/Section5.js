import React, { Component } from 'react';


class Section5 extends Component{

    render() {
    return (


        <section className="probootstrap-section">
        <div className="container">
           <div className="row text-center mb100">
            <div className="col-md-8 col-md-offset-2 probootstrap-section-heading">
              <h2 className="mb30 text-black probootstrap-heading">Try It Today</h2>
              <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
              <p><a href="#" className="btn btn-primary">Get It Now</a></p>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <div className="row">
                <div className="col-md-12">
                  <p><img src="img/laptop_1.jpg" alt="Free Template by uicookies.com" className="img-responsive" /></p>
                </div>
              </div>
              <div className="row">
                <div className="col-md-6">
                  <h4 className="text-black probootstrap-check-2"> What is Instant?</h4>
                  <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
  
                  <h4 className="text-black probootstrap-check-2">How do I use the new features of Frame App?</h4>
                  <p>On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word "and" and the Little Blind Text should turn around and return to its own, safe country. </p>
                </div>
                <div className="col-md-6">
                  <h4 className="text-black probootstrap-check-2">Is this available to my country?</h4>
                  <p>The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way.</p>
  
                  <h4 className="text-black probootstrap-check-2">I have technical problem who do I email?</h4>
                  <p>But nothing the copy said could convince her and so it didn’t take long until a few insidious Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their agency, where they abused her for their.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

        );
    }
}

export default Section5;