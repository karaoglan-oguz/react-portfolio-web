import React, { Component } from 'react';

class Section2 extends Component {

  render() {
    return (

<section className="probootstrap-section probootstrap-bg-light" id="features" data-section="features">
      <div className="container">
        <div className="row text-center mb100">
          <div className="col-md-8 col-md-offset-2 probootstrap-section-heading">
            <h2 className="mb30 text-black probootstrap-heading">Features</h2>
            
          </div>
        </div>

        <div className="row mb100">
          <div className="col-md-8 col-md-pull-2 probootstrap-animate">
            
          </div>
          <div className="col-md-4 col-md-pull-1 probootstrap-section-heading">
            <h3 className="text-primary probootstrap-heading-2">Big Benefits for Small Business</h3>
            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
            <ul className="probootstrap-list">
              <li className="probootstrap-check">Pointing has no control</li>
              <li className="probootstrap-check">A small river named Duden flows</li>
              <li className="probootstrap-check">Roasted parts of sentences fly into your mouth</li>
            </ul>
          </div>
        </div>

        <div className="row mb100">
          <div className="col-md-8 col-md-push-5 probootstrap-animate">
            <p><img src="img/img_showcase_2.jpg" alt="Free Template by uicookies.com" className="img-responsive probootstrap-shadow" /></p>
          </div>
          <div className="col-md-4 col-md-pull-8 probootstrap-section-heading">
            <h3 className="text-primary probootstrap-heading-2">How Frame is different</h3>
            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
            <ul className="probootstrap-list">
              <li className="probootstrap-check">Pointing has no control</li>
              <li className="probootstrap-check">A small river named Duden flows</li>
              <li className="probootstrap-check">Roasted parts of sentences fly into your mouth</li>
            </ul>
          </div>
        </div>

        <div className="row mb100">
          <div className="col-md-8 col-md-pull-2 probootstrap-animate">
            <p><img src="img/img_showcase_1.jpg" alt="Free Template by uicookies.com" className="img-responsive probootstrap-shadow" /></p>
          </div>
          <div className="col-md-4 col-md-pull-1 probootstrap-section-heading">
            <h3 className="text-primary probootstrap-heading-2">All in One Place</h3>
            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
            <ul className="probootstrap-list">
              <li className="probootstrap-check">Pointing has no control</li>
              <li className="probootstrap-check">A small river named Duden flows</li>
              <li className="probootstrap-check">Roasted parts of sentences fly into your mouth</li>
            </ul>
          </div>
        </div>
      </div>
</section>

);
}
}

export default Section2;