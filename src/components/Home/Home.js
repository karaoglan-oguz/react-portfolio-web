import React, { Component } from 'react';
import Nav from './Nav';
import Hero from './../Hero/Hero';
import Section1 from './../section/Section1.js';
import Section2 from './../section/Section2';
import Section3 from './../section/Section3';
import Section4 from './../section/Section4';
import Section5 from './../section/Section5';
import Section6 from './../section/Section6';
import Contact from './../section/Contact';
import Footer from './../Footer/Footer';

class Home extends Component {

  render() {
    return (
      <div>
        <Nav/>
        <Hero/>
        <Section1/>
        <Section2/>
        <Section3/>
        <Section4/>
        <Section5/>
        <Section6/>
        <Contact/>
        <Footer/>
      </div>
    );
  }
}

export default Home;
