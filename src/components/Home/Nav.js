import React, { Component } from 'react';

class Nav extends Component {

  render() {
    return (
      <div>
         <nav className="navbar navbar-default probootstrap-navbar">
            <div className="container">
                <div className="navbar-header">
                <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false" aria-controls="navbar">
                    <span className="sr-only">Toggle navigation</span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                </button>
                <a className="navbar-brand" href="index.html" title="uiCookies:Frame">Frame</a>
                </div>

                <div id="navbar-collapse" className="navbar-collapse collapse">
                <ul className="nav navbar-nav navbar-right">
                    <li className="active"><a href="#" data-nav-section="home">Home</a></li>
                    <li><a href="#" data-nav-section="features">Features</a></li>
                    <li><a href="#" data-nav-section="pricing">Pricing</a></li>
                    <li><a href="#" data-nav-section="reviews">Reviews</a></li>
                    <li><a href="#" data-nav-section="contact">Contact</a></li>
                </ul>
                </div>
            </div>
        </nav>
      </div>
    );
  } 
}

export default Nav;