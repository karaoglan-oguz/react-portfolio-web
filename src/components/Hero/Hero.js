import React, { Component } from 'react';

class Hero extends Component {

  render() {
    return (


        <section className="probootstrap-hero prohttp://localhost/probootstrap/frame/#featuresbootstrap-slant" data-section="home" data-stellar-background-ratio="0.5">
            <div className="container">
                <div className="row intro-text">
                <div className="col-md-8 col-md-offset-2 text-center">
                    <h1 className="probootstrap-heading">uicookies.com Creates High Quality Bootstrap Template For <em>Free</em></h1>
                    <div className="probootstrap-subheading center">
                    <p><a href="https://uicookies.com/" target="_blank" role="button" className="btn btn-primary">Get Them Now</a><a href="#features" className="btn btn-default smoothscroll" role="button">See Features</a></p>
                    </div>
                </div>
                </div>
            </div>
            </section>


        );
    } 
}

export default Hero;