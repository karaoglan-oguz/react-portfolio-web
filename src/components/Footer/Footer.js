import React, { Component } from 'react';


class Footer extends Component{

    render()  {
    return (
        <div>
        <footer className="probootstrap-footer">
        <div className="container text-center">
        <div className="row">
            <div className="col-md-12">
            <p className="probootstrap-social"><a href="#">
            <i className="icon-twitter"></i></a> 
            <a href="#">
            <i className="icon-facebook2"></i></a> 
            <a href="#"><i className="icon-instagram2"></i></a>
            <a href="#"><i className="icon-linkedin"></i></a></p>
            </div>
        </div>
        <div className="row">
            <div className="col-md-12">
            &copy; 2019 Frame. All Rights Reserved. <br/> Designed &amp; <a href="https://uicookies.com/bootstrap-html-templates/">Bootstrap templates </a> by uiCookies <br/> Demo Images by <a href="https://unsplash.com">Unsplash</a>
            </div>
        </div>
        </div>
        </footer>
        </div>

);
}
}
export default Footer;