import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Home from './components/Home/Home.js';


class App extends Component {

  render() {
    return (
      <div>
        <Home/>
      </div>
    );
  }
}

export default App;
